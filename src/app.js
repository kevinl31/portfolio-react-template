import React from "react";
import ReactDOM from "react-dom";

import "normalize.css/normalize.css"
import "./styles/style.scss"

import content from "./content.json"

import Header from "./components/Header";
import PageBody from "./components/PageBody";
import Footer from "./components/Footer";
import NavTabs from "./components/NavTabs";
import HomeTooltip from "./components/HomeTooltip"

const mainApp = <div>
    <Header />
    <NavTabs navLinks={content.navLinks}/>
    <PageBody content={content}/>
    <HomeTooltip />
    <Footer />
</div>

ReactDOM.render(mainApp, document.getElementById('app'))