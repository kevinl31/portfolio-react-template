import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';

const styles = {
    tooltip: {
        position: "fixed",
        bottom: "30px",
        right: " 30px",
    },
    
};

const HomeTooltips = (props) => {

    const { classes } = props;
    return (
        <div className={classes.tooltip}>
            <Tooltip title="Go to top">
                <Button variant="fab" color="primary" href="/#header">
                    <i style={{fontSize: "x-large"}} className="fas fa-arrow-alt-circle-up"></i>
                </Button>
            </Tooltip>
        </div>
    );
};

HomeTooltips.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(HomeTooltips);