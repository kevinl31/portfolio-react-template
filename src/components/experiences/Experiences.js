import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import ExperienceCard from "./ExperienceCard";
import SectionHeader from "../global/SectionHeader"

const styles = (theme) => ({
    root: {

    },
});

const Experiences = (props) => {
    // const { classes } = props;

    return (
        <div>
            <SectionHeader headerContent="My Experiences"/>
            {
                props.experiences.map((item) => (   
                    <ExperienceCard key={item.title} experience={item}/>
                ))
            }
        </div>
    );
}

Experiences.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Experiences);