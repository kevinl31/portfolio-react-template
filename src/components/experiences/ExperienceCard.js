import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const styles = {
  card: {
    minWidth: 275,
    marginTop: "20px",
  },
  cardContainer: {
    display: "flex",
    flexFlow: "row",
    alignItems: "center"
  },
  title: {
    marginBottom: 16,
    fontSize: 20,
  },
  media: {
    width:200,
    height:200,
    marginLeft: "20px"
  },
  listItem: {
    paddingTop: "2px",
    paddingBottom: "2px"
  }
};

const Experience = (props) => {
  const { classes } = props;
  return (
    <Card className={classes.card}>
        <div className={classes.cardContainer}>
          <img className={classes.media} src={props.experience.imagePath} alt={props.experience.imageAlt} />

            <CardContent>
            <Typography variant="headline" component="h4">
              {props.experience.title}
            </Typography>
            <Typography component="p">
              {props.experience.text}
            </Typography>
            <List component="nav">
              {
                props.experience.tasks.map((item, index) => (
                    <ListItem key={index} className={classes.listItem}>
                      <i className="fas fa-angle-right" ></i>
                      <ListItemText 
                        primary= {
                          <Typography component="p">
                            {item}
                          </Typography>
                        } 
                        disableTypography={true}
                      />
                    </ListItem>
                ))
              }
            </List>
            {
              props.experience.subText && <Typography component="p">
                {props.experience.subText}
              </Typography>
            }
            {
              props.experience.tools && <Typography component="p">
                Tools used: {props.experience.tools.join(", ")}
              </Typography>
            }
            </CardContent>
        </div>
    </Card>
  );
}

Experience.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Experience);