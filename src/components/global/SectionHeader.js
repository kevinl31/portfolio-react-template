import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = {
    root: {
        marginBottom: "20px"
    }
};

const SectionHeader = (props) => {
    const { classes } = props;

    return (
        <Typography classes={{root: classes.root}}variant="display1" align="center">
            {props.headerContent}
        </Typography>
    );
};

SectionHeader.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SectionHeader);