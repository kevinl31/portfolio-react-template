import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';


import ThingIDoCard from "./ThingIDoCard";
import SectionHeader from "../global/SectionHeader"

const styles = () => ({
    root: {
        
    },
});

const ThingsIDoPaper = (props) => {
    // const { classes } = props;

    return (
        <div id="thingsIDo">
            <SectionHeader headerContent="Things I Do"/>
            <Grid container spacing={24}>
                {
                    props.thingsIDo.map((item) => (
                        <Grid key={item.title} item xs={4}>
                            <ThingIDoCard thingIDo={item}/>
                        </Grid>
                    ))
                }
            </Grid>
        </div>
    );
}

ThingsIDoPaper.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ThingsIDoPaper);