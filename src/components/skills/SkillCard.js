import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const styles = {
  root: {
    display: "inline"
  },
  card: {
    minWidth: 275,
  },
  title: {
    marginBottom: 16,
    fontSize: 20,
  },
  media: {
    height: 140,
  },
  linear: {
    height: 8
  }
};

const Skill = (props) => {
  const { classes } = props;
  return (
    <Card className={classes.card}>
        <CardHeader title={props.skill.title} classes={{title: classes.title}}></CardHeader>
        <CardContent>
          {props.skill.text && <Typography component="p">{props.skill.text}</Typography>}  
          
          <List component="nav">
            {props.skill.skillsList.map((item) => (
              <ListItem key={item.name} classes={{root: classes.root}}>
                <ListItemText primary={item.name} />
                {item.level && <LinearProgress classes={{root: classes.linear}} variant="determinate" value={item.level} />}
              </ListItem>
            ))}
            
          </List>
      </CardContent>
    </Card>
  );
}

Skill.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Skill);