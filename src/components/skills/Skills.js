import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import SectionHeader from "../global/SectionHeader"
import SkillCard from "./SkillCard";

const styles = () => ({
    root: {
        display: "inline"
    },
});

const Skills = (props) => {
    const { classes } = props;
    return (
        <div>
            <SectionHeader headerContent="My Skills"/>
            <Grid container spacing={24}>
                {
                    props.skills.map((item) => (
                        <Grid key={item.title} item xs={4}>   
                            <SkillCard skill={item}/>
                        </Grid>
                    ))
                }
            </Grid>
        </div>
    );
}

Skills.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Skills);