import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid"
import PersonalProjectCard from "./PersonalProjectCard";
import SectionHeader from "../global/SectionHeader"

const styles = (theme) => ({
    root: {
        padding: "2px"
    },
});

const PersonalProjects = (props) => {
    const { classes } = props;

    return (
        <div>
            <SectionHeader headerContent="My Personal Project"/>

            <Grid container spacing={24}>
                {
                    props.projects.map((item) => (
                        <Grid key={item.title} item xs={6}>   
                            <PersonalProjectCard project={item}/>
                        </Grid>
                    ))
                }
            </Grid>
        </div>
    );
}

PersonalProjects.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PersonalProjects);