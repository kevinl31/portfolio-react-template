import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions'
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const styles = {
    card: {
        minWidth: 275,
      },
      title: {
        marginBottom: 16,
        fontSize: 20,
      },
      media: {
        height: 140,
    }
};

const PersonalProject = (props) => {
    const { classes } = props;
  
    return (
        <Card className={classes.card}>
        <CardHeader title={props.project.title} classes={{title: classes.title}}></CardHeader>
        <CardMedia
          className={classes.media}
          image={props.project.imagePath}
          title={props.project.imageAlt}
        />
        <CardContent>
          <Typography component="p">
            {props.project.text}
          </Typography>
      </CardContent>
      {props.project.stack && 
        <CardContent>
          <Typography component="p">
            Made with {props.project.stack.join(", ")}
          </Typography>
        </CardContent>
      }
      <CardActions>
        <Button size="small" href={props.project.ReadMoreLink}>Learn More</Button>
      </CardActions>
    </Card>
    );
  }

PersonalProject.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PersonalProject);