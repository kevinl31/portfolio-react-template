import React from 'react';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

class NavTabs extends React.Component {
  state = {
    value: 2,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    return (
      <Paper square>
        <Tabs
            value={this.state.value}
            indicatorColor="primary"
            textColor="primary"
            onChange={this.handleChange}
            centered={true}
        >
          {
            this.props.navLinks.map((item, index) => (
              <Tab key={index} label={item.text} href={item.link} />
            ))
          }
        </Tabs>
      </Paper>
    );
  }
}

export default NavTabs