import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = {
    footer: {
        padding: "10px",
        backgroundColor: "#454545",
        textAlign: "center",
        color: "#ececec",
        marginTop: "20px"
    },
    linkFooter: {
        color: "#ececec"
    }
}

const Footer = (props) => {
    const { classes } = props;

    return (
        <footer className={classes.footer}>
            <div className="container">
                <Typography component="p" color="inherit">
                    Made with <i className="fas fa-heart"></i> by Kevin Letupe with React.js and Material-UI.
                </Typography>
                <Typography component="p" color="inherit">
                    You like this template? Feel free to <a className={classes.linkFooter} href="https://github.com/KevinL59">reuse it</a>.
                </Typography> 
            </div>
        </footer>
    );
};

Footer.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Footer);;