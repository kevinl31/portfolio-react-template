import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';

import AboutCard from "./AboutCard";
import PingMeCard from "./PingMeCard";
import ThingsIDoPaper from './thingsIDo/ThingsIDo';
import Experiences from "./experiences/Experiences";
import Skills from "./skills/Skills";
import PersonalProjects from './personalProjects/PersonalProjects';
import HomeTooltip from "./HomeTooltip"

import content from "../content.json"

const PageBody = () => {    
    return (
        <section id="main" className="main">
            <div>
                <Grid container spacing={24}>
                    <Grid item xs={8}>
                        <AboutCard about={content.about}/>
                    </Grid>
                    <Grid item xs={4}>
                        <PingMeCard />
                    </Grid>
                    <Grid item xs={12}>
                        <ThingsIDoPaper thingsIDo={content.thingsIDo}/>
                    </Grid>
                    <Grid item xs={12}>
                        <Experiences experiences={content.experiences}/>
                    </Grid>
                    <Grid item xs={12}>
                        <Skills skills={content.skills}/>
                    </Grid>
                    <Grid item xs={12}>
                        <PersonalProjects projects={content.projects}/>
                    </Grid>
                </Grid>
            </div>
        </section>
    );
};

export default PageBody;