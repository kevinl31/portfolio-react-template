import React, { Component } from 'react';
import Button from '@material-ui/core/Button';


const Header = () => {
    return (
        <header id="header">
            <div className="container">
                <img className="profile-photo" src="/images/profile-picture.jpg" alt="Kevin Letupe: Full Stack Engineer" />
                <div className="header-content">
                    <div className="header-details">
                        <h1>Kevin Letupe</h1>
                        <h2>Freelace Developper Full Stack</h2>
                        <ul className="social-icons">
                            <li><a href="https://twitter.com/kevinLtp59"><i className="fab fa-twitter brand-icon"></i></a></li>
                            <li><a href="https://www.linkedin.com/in/kletupe60/"><i className="fab fa-linkedin brand-icon"></i></a></li>
                            <li><a href="https://github.com/KevinL59"><i className="fab fa-github brand-icon"></i></a></li>
                        </ul>
                    </div>
                    <Button variant="contained" color="primary" className="contact-me" href="mailto:kevin.letupe@gmail.com">
                        <i className="fa fa-paper-plane"></i> Contact Me
                    </Button>
                </div>
            </div>
        </header>
    );
};

export default Header;