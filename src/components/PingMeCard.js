import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

const styles = {
  card: {
    minWidth: 275,
  },
  title: {
    marginBottom: 16,
    fontSize: 20,
  },
  media: {
    height: 140,
  }
};

const PingMeCard = (props) => {
  const { classes } = props;
  return (
    <Card className={classes.card}>
        <CardHeader title="Ping Me" classes={{title: classes.title}}></CardHeader>
        
        <CardContent>
        <form noValidate autoComplete="off">
        <TextField
          id="name"
          label="Name"
          margin="normal"
        />
        <TextField
          id="email"
          label="Email Address"
          defaultValue="contact@gmail.com"
          margin="normal"
        />
        <TextField
          id="message"
          label="Message"
          defaultValue="Your Message"
          margin="normal"
          multiline
          rowsMax="10"
        />
        </form>
      </CardContent>
    </Card>
  );
}

PingMeCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PingMeCard);
