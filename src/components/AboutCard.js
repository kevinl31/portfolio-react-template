import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    minWidth: 275,
  },
  title: {
    marginBottom: 16,
    fontSize: 20,
  },
  media: {
    height: 140,
  }
};

const AboutCard = (props) => {
  const { classes } = props;
  return (
    <Card className={classes.card}>
        <CardHeader title={props.about.title} classes={{title: classes.title}}></CardHeader>
        <CardMedia
          className={classes.media}
          image={props.about.imagePath}
          title={props.about.imageAlt}
        />
        <CardContent>
          <Typography component="p">
            {props.about.text}
          </Typography>
      </CardContent>
    </Card>
  );
}

AboutCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AboutCard);
